package com.kmwllc.chatlantis.intent;

import com.kmwllc.chatlantis.Utils;
import com.kmwllc.chatlantis.bot.Bot;
import com.kmwllc.chatlantis.context.ContextConstants;
import com.kmwllc.chatlantis.context.FullContext;

import java.util.Map;

/**
 * Basic implementation of IntentMatcher which expects the Intent to be spelled out
 * explicitly in Context.  It checks in the "intent.name" slot in the Utterance Context
 * and gives the Intent named there a value of 100% confidence.
 */
public class ExplicitIntentMatcher implements IntentMatcher, ContextConstants, Utils {
  
  @Override
  public Map<Intent, Double> assignIntent(FullContext ctx, Bot bot) {
    String intentName = (String) ctx.get(fmt("$UTT:/%s.name", INTENT));
    Intent intent = bot.getIntents().get(intentName);
    return mapOf(intent, 100d);
  }
}
