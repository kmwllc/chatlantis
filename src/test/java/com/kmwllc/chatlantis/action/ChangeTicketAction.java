package com.kmwllc.chatlantis.action;

import com.kmwllc.chatlantis.Utils;
import com.kmwllc.chatlantis.bot.Bot;
import com.kmwllc.chatlantis.context.FullContext;
import com.kmwllc.chatlantis.context.SubExpression;
import com.kmwllc.chatlantis.intent.Intent;
import com.kmwllc.chatlantis.lookup.instruction.CompleteIntent;
import com.kmwllc.chatlantis.lookup.instruction.Instruction;
import com.kmwllc.chatlantis.lookup.instruction.InstructionContext;
import com.kmwllc.chatlantis.lookup.instruction.SetStringSlot;
import com.kmwllc.chatlantis.validation.Constraint;
import com.kmwllc.chatlantis.validation.Validator;
import com.kmwllc.chatlantis.validation.Violation;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class ChangeTicketAction implements Action, Utils {
  @Override
  public List<Instruction> act(FullContext ctx, Bot bot) {
    boolean valid = true;
    StringBuilder errMsg = new StringBuilder();
    String slotRef = (String) ctx.get("$utt:/objects/changeTicket.property");
    String slotValue = (String) ctx.get("$utt:/objects/changeTicket.value");
    Intent createTicketIntent = bot.getIntent("createTicket");
    List<Constraint> constraints = createTicketIntent.getConstraintsForSlot(slotRef);
    for (Constraint c : constraints) {
      Validator v = c.getValidator();
      Set<Violation> violations = v.validateString(slotValue, ctx, c.getPrompt(), c.getPath(),
          c.getErrorMessage());
      for (Violation violation : violations) {
        valid = false;
        errMsg.append(violation.getErrorMessage());
      }
    }
    if (!valid) {
      return listOf(new SetStringSlot("/action.spoken", errMsg.toString()));
    } else {
      SubExpression subEx = getSubEx(slotRef);
      InstructionContext ic = subEx.getInstructionContext();
      String subRef = subEx.getExpr();
      return listOf(new SetStringSlot(ic, subRef, slotValue), new CompleteIntent());
    }
  }
  
  @Override
  public void init(Map<String, String> map) {
  
  }
}
