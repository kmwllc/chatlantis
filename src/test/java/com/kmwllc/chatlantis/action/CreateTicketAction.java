package com.kmwllc.chatlantis.action;

import com.kmwllc.chatlantis.Utils;
import com.kmwllc.chatlantis.bot.Bot;
import com.kmwllc.chatlantis.context.FullContext;
import com.kmwllc.chatlantis.lookup.instruction.Instruction;
import com.kmwllc.chatlantis.lookup.instruction.SetStringSlot;

import java.util.List;
import java.util.Map;


public class CreateTicketAction implements Action, Utils {
  private int numTix;
  
  @Override
  public List<Instruction> act(FullContext ctx, Bot bot) {
    String tixId = fmt("KMW-%03d", ++numTix);
    String response = fmt("Created ticket %s in JIRA", tixId);
    return listOf(new SetStringSlot("/objects/ticket.number", tixId),
        new SetStringSlot("/action.spoken", response));
  }
  
  @Override
  public void init(Map<String, String> map) {
    numTix = 0;
  }
}

