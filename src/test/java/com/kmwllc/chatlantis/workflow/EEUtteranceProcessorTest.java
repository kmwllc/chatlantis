package com.kmwllc.chatlantis.workflow;

import com.kmwllc.chatlantis.Utils;
import com.kmwllc.chatlantis.utterance.Utterance;
import com.kmwllc.chatlantis.utterance.UtteranceFactory;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static com.kmwllc.chatlantis.workflow.EEUtteranceProcessor.ALIAS_PROP;
import static com.kmwllc.chatlantis.workflow.EEUtteranceProcessor.DICT_FILE_PROP;
import static org.junit.Assert.assertEquals;

public class EEUtteranceProcessorTest implements Utils {
  
  @Test
  public void testEEUtteranceProcessor() throws IOException {
    EEUtteranceProcessor ee = new EEUtteranceProcessor();
    ee.init(stringMapOf(DICT_FILE_PROP, "employee-dict.csv", ALIAS_PROP, "EMP"));
    Utterance initU = UtteranceFactory.get().createNew("matt, kevin and brian are kmw employees",
        null);
    List<Utterance> utts = ee.process(initU);
    assertEquals(8, utts.size());
  }

  @Test
  public void testEntityPayload() throws IOException {
    EEUtteranceProcessor ee = new EEUtteranceProcessor();
    ee.init(stringMapOf(DICT_FILE_PROP, "states.csv", ALIAS_PROP, "STATE"));
    Utterance initU = UtteranceFactory.get().createNew("MA", null);
    List<Utterance> utts = ee.process(initU);
    assertEquals(2, utts.size());
    assertEquals("massachusetts", utts.get(1).getToken(0).getValue());
  }
}
